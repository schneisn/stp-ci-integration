package main

import (
	"bytes"
	"flag"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

func main() {

	url := flag.String("url", "", "the url of the request")
	version := flag.String("version", "", "the system-under-test-version")
	variant := flag.String("variant", "Default", "the system-under-test-variant")
	token := flag.String("token", "", "the secret token for the system-under-test")
	sleepTime := flag.Duration("time", time.Duration(300*time.Second), "the sleep time in seconds")
	protocolPath := flag.String("protocolpath", "", "were to save the protocols")
	flag.Parse()

	if *url == "" {
		log.Fatal("please enter a valid url")
	}

	if *variant == "" {
		*variant = "Default"
	}

	success := polling(url, version, variant, token, sleepTime, *protocolPath)
	if !success {
		log.Fatal("tests were not successful")
	}

	log.Println("tests were successful")
	os.Exit(0)
}

// polling sends the requests to the SystemTestPortal every `sleepTime`-seconds.
func polling(url, version, variant, token *string, sleepTime *time.Duration, protocolPath string) bool {
	// The failCounter sets the maximum amount of
	// allowed failures for sending the request
	failCounter := 10

	for failCounter > 0 {
		resp, err := http.Post(parseURL(*url), "application/json", createBody(*version, *variant, *token))
		if err != nil {
			log.Println(err)
			failCounter--
		} else if internalError(resp) {
			log.Println("internal server error of the SystemTestPortal")
			failCounter--
		} else if testsDone(resp) {
			saveZip(resp, protocolPath)
			if testsSuccessful(resp) {
				return true
			} else {
				return false
			}
		} else {
			log.Println("not all tests executed yet")
		}

		time.Sleep(*sleepTime)
	}
	return false
}

// saveZip saves a zip-archive with all protocols as pdf
// under the name "testresults.zip".
func saveZip(response *http.Response, protocolPath string) error {
	if protocolPath != "" && !strings.HasSuffix(protocolPath, "/") {
		protocolPath += protocolPath + "/"
	}
	file, err := os.Create(protocolPath + "testresults.zip")
	if err != nil {
		return err
	}

	defer file.Close()
	io.Copy(file, response.Body)

	return nil
}

// testsDone checks the statuscode of the response.
// StatusCode 404 indicates that not all tests
// are executed yet.
func testsDone(resp *http.Response) bool {
	if resp.StatusCode != http.StatusNotFound {
		return true
	}
	return false
}

// testsSuccessful checks if the response indicates
// successful tests. Status 200 indicates successful tests.
func testsSuccessful(resp *http.Response) bool {
	if resp.StatusCode == http.StatusOK {
		return true
	}
	return false
}

// internalError checks if the response suggests an
// internal server error of the stp.
func internalError(resp *http.Response) bool {
	if resp.StatusCode == http.StatusInternalServerError {
		return true
	}
	return false
}

// parseURL adds https prefix to the url.
//
// If this prefix is missing, the request cannot be sent
// due to an "unsupported protocol scheme"-error.
//
// Returns the parsed string
func parseURL(url string) string {
	if !strings.HasPrefix(url, "http") {
		url = "https://" + url
	}
	return url
}

// createBody returns a json body with the version, variant and token
func createBody(version, variant, token string) *bytes.Buffer {
	return bytes.NewBuffer([]byte("{\"version\":\"" + version + "\", " +
		"\"variant\":\"" + variant + "\"," +
		"\"token\":\"" + token + "\"}"))
}
