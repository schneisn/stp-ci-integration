FROM frolvlad/alpine-glibc

LABEL version="0.1"
LABEL description="Image for integrating the SystemTestPortal into the CI"

WORKDIR /app

# Only add binary
ADD stp-ci-integration /app/

# App runs on port 4200
EXPOSE 4200

# Add bash to image
RUN apk add --no-cache bash

ENTRYPOINT ["/bin/bash","-c"]

# The flags url, version and token will be overridden in the respective projects
CMD stp-ci-integration --url=$stp-url --version=$stp-version --variant=$stp-variant --token=$stp-token
