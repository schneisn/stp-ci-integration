[![pipeline status](https://gitlab.com/schneisn/stp-ci-integration/badges/master/pipeline.svg)](https://gitlab.com/schneisn/stp-ci-integration/commits/master)

# SystemTestPortal - CI/CD Integration

This application handles the integration of the [SystemTestPortal](https://gitlab.com/stp-team/systemtestportal-webapp) into the Continuous Integration / Continous Deployment Pipeline.

## Why do we need an integration of manual tests into the CI/CD - pipeline?

The integration provides a way to do manual tests in continuous delivery/continuous deployment. It is an extra step for the quality assurance of your project.

## How to integrate the SystemTestPortal into the CI/CD - pipeline in GitLab?

- Add this to your .gitlab-ci.yaml to add a stage with manual testing for your tags  

```
stages:
  - stp-ci-integration

stp-ci-integration:
  image: registry.gitlab.com/schneisn/stp-ci-integration:latest
  stage: stp-ci-integration
  script:
    - cp /app/stp-ci-integration $CI_PROJECT_DIR/stp-ci-integration
    - cd $CI_PROJECT_DIR/
    - ./stp-ci-integration --url="$STP_URL" --version="$CI_COMMIT_TAG" --variant="$STP_VARIANT" --token="$STP_TOKEN" --time="30s"
  only:
    - tags
  artifacts:
    when: always
    paths:
      - testresults.zip
```
- Go to the settings of your project in the SystemTestPortal and add the `STP_URL` and the `STP_TOKEN` to the environment variables of your GitLab-project. If you want to test a specific variant of your system-under-test, add a `STP_VARIANT` to your environment variables. If this variable is not set, the variant "Default" will be tested.

- Done! Everytime you release a version and your pipeline runs, this application communicates with the SystemTestPortal to handle manually testing your application during the pipeline.

## How does this application work?

1. This app needs some parameters to work:
  - `--url=<request-url>` The url of your project in the SystemTestPortal where the request is sent to.  
      The form of `<request-url>` is `"host"/"owner"/"project"/ci`:
     - `"host"` host of the SystemTestPortal
     - `"owner"` owner of the project in the SystemTestPortal
     - `"project"` the name of the project in the SystemTestPortal
  - `--version=<version>` The version of your system-under-test that you want to test
  - `--variant=<variant>` The variant of you system-under-test that you want to test. Default value is "Default".
  - `--token=<token>` The secret token that is in your projects settings in the SystemTestPortal
  - `--time=<time (0h0m0s)>` the sleep-time that defines how often a request is sent to the SystemTestPortal. Default is 300s.
  - `--protocolpath=<path/to/store/protocols>` The path to store protocols. Can be absolute or relative. Default is same path as the server.

2. The app sends requests to the SystemTestPortal periodically to check the status of the tests for the given version and variant.  
   The SystemTestPortal responds with a response-code that depends on the current status of the tests.  
   There are several possible response codes:
     - OK (200) -> Successful tests, this app exits with exit-code 0
     - BAD REQUEST (400) -> Failed tests, this app exits with exit-code 1
     - NOT FOUND (404) -> the tests are not yet executed -> continue sending requests
    
3. Meanwhile, the SystemTestPortal handles the manual tests of your application.

## Applicability for the SystemTestPortal

The CI/CD-Integration is supported as of version 1.7.0 of the SystemTestPortal
